# tensorflow-docker-retrain

`tensorflow-docker-retrain` uses the [TensorFlow](https://www.tensorflow.org/) framework to retrain an existing [InceptionV3](https://github.com/tensorflow/models/tree/master/research/inception) classifier via [Docker](https://www.docker.com/) without ever having to install TensorFlow.

You can provide your own training images from any source by creating the appropriate directory structure. This repository optionally provides the ability to split a video into individual frames that can be used to quickly gather a significant number of training images.

This repository is inspired from
[How to Retrain an Image Classifier for New Categories](https://www.tensorflow.org/hub/tutorials/image_retraining)
TensorFlow official tutorial.

**For a full tutorial on how this works and how to use it, [click here](https://kylewbanks.com/blog/tutorial-transfer-learning-retraining-inception-mobilenet-with-tensorflow-and-docker).**

## In preparation

- Use training set from https://www.kaggle.com/jessicali9530/stanford-cars-dataset/version/2 to have a more complete example

- Create flask API to classify images received through HTTP on the fly

## Preparing Training Data from Videos

1. Place your **.mp4** video files in [frame-extraction/videos/](./frame-extraction/videos/)
2. `docker build -t frame-extraction frame-extraction/`
3. `docker run -it -v $(pwd)/frame-extraction:/data frame-extraction`
4. Retrieve the split video frames from [frame-extraction/frames/](./frame-extraction/frames), and place in the appropriate directory structure.

## Building input directory structure

You should have a folder containing class-named subfolders, each full
of images for each label. An example folder could have a structure like this:

    ~/train_input/back/168136513edzddze4984.jpg
    ~/train_input/front/4231de1fdz35fz3ef1zs.jpg
    ~/train_input/back/zemjffnesoinfsgnrg.jpg
    ~/train_input/left/foriegeongvttgojg.jpg

The **subfolder names are important**, since *they define what label is applied to
each image*, but the **filenames themselves don't matter**.

## Training the Classifier

Before training you must build the generic `train-classifier` Docker image:

```sh
$ docker build -t train-classifier train-classifier/
```

Next invoke the **train-classifier** image by mounting the directory containing your training images as **/input** and an output directory as **/output** which will contain the trained model and labels text file after successful training.

```sh
$ docker run -it \
    -v $(pwd)/path/to/training-images:/input \
    -v $(pwd)/output:/output \
    train-classifier
```

### Using the [Makefile](https://gitlab.com/gu.charbon/tensorflow-docker-retrain/blob/master/Makefile)

```sh
make train
```


## Performing Predictions

Once you have a trained model, you can use it to perform predictions like so:

```sh
$ docker build -t predictor predictor/
```

- Predict all images in input folder
```
$ docker run -it \
    -v $(pwd)/path/to/model/dir/:/model \
    -v $(pwd)/test_input:/input \
    predictor
```
**Note:** You need to mount two volumes here:

1. `/model` is the path to your trained model and labels, the equivalent of **/output/tf_files** from the training step above.
2. `/input` is the folder that contains your image to predict.

> Note: I didn't test this syntax for the moment.

- Predict only one image

```
$ docker run -it \
    -v $(pwd)/path/to/model/dir/:/model \
    -v $(pwd)/test_input:/input \
    predictor \
    image_name.jpg
```

> Note: `image_name` has to be placed in the `test_input` directory.

### Using the [Makefile](https://gitlab.com/gu.charbon/tensorflow-docker-retrain/blob/master/Makefile)

```sh
make && make test_predict_output
```

> Note: The first make builds the required docker images, then train the model.
The second make with target `test_predict_output` will execute the model on the test image.


## Examples

We want to perform classification on cars images.
For each image we want to know if it's a view of:

- the front of the car
- the back of the car
- the left side of the car
- the right side of the car


After learning on 32 images of cars for each category (front, back, left, right),
we perform a prediction on the following photo:

![Front view of a BMW](https://gitlab.com/gu.charbon/tensorflow-docker-retrain/raw/master/test_input/voiture-sans-plaque-avant.jpg?inline=false)

```shell
make && make test_predict_output
```

The results are:

```shell
  front 0.65173835
  back 0.27989122
  right 0.039346527
  left 0.02902398
```

The score would surely increase greatly if the model was trained on larger dataset.

## Author

- Guillaume Charbonnier

## Original Author

- [Kyle Banks](https://kylewbanks.com/blog)

## License

```
MIT License

Copyright (c) 2018 Guillaume Charbonnier

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
