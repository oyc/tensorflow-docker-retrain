.DEFAULT_GOAL := requirements

VERSION	:= 1.0
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
CUR_DATE := $(shell date +"%s")


TEST_INPUT_DIR := $(ROOT_DIR)/test_input
TRAIN_INPUT_DIR := $(ROOT_DIR)/train_input
MODEL_DIR := $(ROOT_DIR)/model_output


.PHONY: requirements train predict all clean extract_images


extract_images:
	docker build -t frame-extraction frame-extraction/ && \
	docker run -i \
	-v $(ROOT_DIR)/frame-extraction:/data frame-extraction

directories:
	mkdir -p $(MODEL_DIR)/tf_files

clean:
	rm -Rf $(MODEL_DIR)

build_training_image:
	docker build -t train-classifier train-classifier/.

train_model:
	docker run -i \
	-v $(TRAIN_INPUT_DIR):/input \
    -v $(MODEL_DIR):/output \
    train-classifier

build_predictor_image:
	docker build -t predictor predictor/

test_predict_output_directory:
	docker run -i \
	-v $(MODEL_DIR)/tf_files:/model \
	-v $(TEST_INPUT_DIR):/input \
	predictor

test_predict_output:
	docker run -i \
	-v $(MODEL_DIR)/tf_files:/model \
	-v $(TEST_INPUT_DIR):/input \
	predictor \
	voiture-sans-plaque-avant.jpg

requirements: train build_predictor_image

train: directories build_training_image train_model

all: requirements train_model test_predict_output
