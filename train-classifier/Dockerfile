FROM tensorflow/tensorflow:1.7.0

MAINTAINER Guillaume Charbonnier <gu.charbon@gmail.com>

ENV OUTPUT_GRAPH tf_files/retrained_graph.pb
ENV OUTPUT_LABELS tf_files/retrained_labels.txt
ENV TFHUB_MODULE https://tfhub.dev/google/imagenet/inception_v3/feature_vector/1
ENV TRAINING_STEPS 200
ENV SUMMARIES_DIR /output/retrain_logs

VOLUME /output
VOLUME /input

RUN pip install --no-cache-dir tensorflow_hub && \
	mkdir -p /tmp/tfhub_cache && \
	mkdir -p "${SUMMARIES_DIR}"

ENV _TFHUB_CACHE_DIR=/tmp/tfhub_cache

RUN curl -O https://raw.githubusercontent.com/tensorflow/hub/master/examples/image_retraining/retrain.py

ENTRYPOINT python retrain.py \
  --how_many_training_steps="${TRAINING_STEPS}" \
  --model_dir=/output/tf_files/models/ \
  --output_graph=/output/"${OUTPUT_GRAPH}" \
  --output_labels=/output/"${OUTPUT_LABELS}" \
  --tfhub_module="${TFHUB_MODULE}" \
  --image_dir=/input \
	--print_misclassified_test_images \
	--summaries_dir="${SUMMARIES_DIR}"
